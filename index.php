<?php //require "envia_correo_contacto.php"; 
  $nombre  = 0;
  $email   = 0;
  $asunto  = 0;
  $mensaje = 0;
  $msg     = 2;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>ECmaster solución en electrónica , comunicación e informática</title>
    <meta name="description" content="ECmaster solución en electrónica , comunicación e informática">
    <link rel="shortcut icon" href="assets/img/favicon.png?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" href="assets/css/width-boxed.min.css" id="ms-boxed" disabled="">
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    
    <div id="ms-preload" class="ms-preload">
      <div id="status">
        <div class="spinner">
          <div class="dot1"></div>
          <div class="dot2"></div>
        </div>
      </div>
    </div>

    <div class="ms-site-container ms-nav-fixed">
      <nav class="navbar navbar-expand-md navbar-fixed ms-lead-navbar navbar-mode navbar-mode mb-0" id="navbar-lead">
        <div class="container container-full">
          <div class="navbar-header">
            <a class="navbar-brand" href="index.html">
              <!-- <img src="assets/img/demo/logo-navbar.png" alt=""> -->
              <span class="ms-logo ms-logo-white ms-logo-sm">EC</span>
              <span class="ms-title">
                <strong>ECmaster</strong>
              </span>
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="nav-item">
                <a data-scroll class="nav-link active" href="#Inicio">Inicio</a>
              </li>
              <li class="nav-item">
                <a data-scroll class="nav-link" href="#Nosotros">Nosotros</a>
              </li>
              <li class="nav-item">
                <a data-scroll class="nav-link" href="#Servicios">Servicios</a>
              </li>
              <li class="nav-item">
                <a data-scroll class="nav-link" href="#Clientes">Clientes</a>
              </li>
              <li class="nav-item">
                <a data-scroll class="nav-link" href="#Contacto">Contacto</a>
              </li>
            </ul>
          </div>
          <!-- navbar-collapse collapse -->
          <a href="javascript:void(0)" class="ms-toggle-left btn-navbar-menu">
            <i class="zmdi zmdi-menu"></i>
          </a>
        </div>
        <!-- container -->
      </nav>
      <div class="intro-full ms-hero-img-city2 ms-hero-bg-primary color-white" id="Inicio">
        <div id="intro-video" class="" data-type="youtube" data-video-id="1t6RsBaY8yQ" data-plyr='{ "autoplay": true, "volume": 0, "controls": [], "loop": true }'></div>
        <div class="intro-full-content index-1">
          <div class="container">
            <div class="text-center mb-4">
              <!--span class="ms-logo ms-logo-lg ms-logo-white center-block mb-2 mt-2 animated zoomInDown animation-delay-5"></span-->
              <h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">  
                <strong>ECmaster</strong><br>
                <span>Solución en electrónica, comunicación e informática</span>
              </h1>
              <p class="lead lead-lg color-white text-center center-block mt-2 mw-800 text-uppercase fw-300 animated fadeInUp animation-delay-7">Para nosotros, Tú eres lo más importante.
                <!--span class="color-warning">rigorous process</span> of creation. Our principles are creativity, design, experience and knowledge.</p-->
            </div>
            <div class="text-center mb-2">
              <a id="go-intro-full-next" href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-white animated zoomInUp animation-delay-12">
                <i class="zmdi zmdi-long-arrow-down"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="btn-back-top">
        <a href="#" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised ">
          <i class="zmdi zmdi-long-arrow-up"></i>
        </a>
      </div>
      <!-- container -->
        <section id="Nosotros" class="mt-6">
      <div class="bg-light index-1 intro-full-next pt-6">
        <div class="container" id="intro-next">
          <h2 class="text-center color-primary mb-2 wow fadeInDown animation-delay-4">Quiénes somos y a dónde vamos.</h2>
          <p class="lead text-center aco wow fadeInDown animation-delay-5 mw-800 center-block mb-4"> Somos una nueva empresa dedicada 100% al desarrollo de Soluciones Tecnológicas, enfocando sus esfuerzos en la atención y satisfacció del cliente, cuidando los detalles de aspecto y funcionalidad, utilizando las últimas modalidades de desarrollo y tendencias de diseño, para entregar un producto atractivo, seguro, funcional y actualizado.
            <!--span class="color-primary">consectetur adipisicing</span> elit. Dolor alias provident excepturi eligendi, nam numquam iusto eum illum, ea quisquam.</p-->
          <div class="row">
            <div class="ms-feature col-xl-3 col-lg-3 col-md-6 col-sm-6 card wow fadeInUp animation-delay-4">
              <div class="text-center card-body">
                <span class="ms-icon ms-icon-circle ms-icon-xxlg color-info">
                  <i class="zmdi zmdi-badge-check"></i>
                </span>
                <h4 class="color-info">Compromiso</h4>
                <p class="">Trabajamos en conjunto con usted, hasta lograr todas las metas.</p>
                <!--a href="javascript:void(0)" class="btn btn-info btn-raised">Action here</a-->
              </div>
            </div>
            <div class="ms-feature col-xl-3 col-lg-3 col-md-6 col-sm-6 card wow fadeInUp animation-delay-8">
              <div class="text-center card-body">
                <span class="ms-icon ms-icon-circle ms-icon-xxlg color-warning">
                  <i class="zmdi zmdi-flower"></i>
                </span>
                <h4 class="color-warning">Empatía</h4>
                <p class="">Entendemos sus requirimientos hasta lograr sus espectativas.</p>
                <!--a href="javascript:void(0)" class="btn btn-warning btn-raised">Action here</a-->
              </div>
            </div>
            <div class="ms-feature col-xl-3 col-lg-3 col-md-6 col-sm-6 card wow fadeInUp animation-delay-10">
              <div class="text-center card-body">
                <span class="ms-icon ms-icon-circle ms-icon-xxlg color-success">
                  <i class="zmdi zmdi-favorite"></i>
                </span>
                <h4 class="color-success">Pasión</h4>
                <p class="">Nos encanta lo que hacemos, ya que es con esmero y es nuestra vocación.</p>
                <!--a href="javascript:void(0)" class="btn btn-success btn-raised">Action here</a-->
              </div>
            </div>
            <div class="ms-feature col-xl-3 col-lg-3 col-md-6 col-sm-6 card wow fadeInUp animation-delay-6">
              <div class="text-center card-body">
                <span class="ms-icon ms-icon-circle ms-icon-xxlg  color-danger">
                  <i class="zmdi zmdi-eye"></i>
                </span>
                <h4 class="color-danger">Humildad</h4>
                <p class="">Reconocemos nuestras fallas, con ánimos de mejorar nuestras debilidades y buscar una mejora continua.</p>
                <!--a href="javascript:void(0)" class="btn btn-danger btn-raised">Action here</a-->
              </div>
            </div>
          </div>
        </div>
        
          <div class="wrap wrap-mountain mt-6">
        <div class="container">
          <h2 class="text-center text-light mb-6 wow fadeInDown animation-delay-5">
            <strong>Beneficios</strong> para su empresa.</h2>
            <h3 class="animated fadeInLeft animation-delay-18">Permita a su empresa trabajar de una manera m&aacute;s eficiente y eficaz. </h3>
          <div class="row">
            <div class="col-lg-6 order-lg-2 mb-4  center-block">
              <img src="assets/img/demo/mock.png" alt="" class="img-fluid center-block wow zoomIn animation-delay-12 "/> 
            </div>
            <div class="col-lg-6 order-lg-1 pr-6">
              <p class="wow fadeInLeft animation-delay-6">En SolucionesWebcon, adoptamos la evolución constante como estrategia competitiva, la cual impacta en los objetivos, planes, metas y proyectos de nuestros clientes. </p>
              <p class="wow fadeInLeft animation-delay-7">Por esta raz&oacute;n nos comprometemos por medio de nuestra soluciones tecnol&oacute;gicas a:</p>
              <p class="wow fadeInLeft animation-delay-8">
                <ul>
                  <li>La reducci&oacute;n de tiempos en procesos.</li>
                  <li>El aumento de la c&aacute;lidad.</li>
                  <li>La reducci&oacute;n de costo en procesos.</li>
                  <li>El aumento de clientes potenciales.</li>
                  <li>Un grado mayor de satisfacci&oacute; en sus clientes.</li>
                  <li>Mejor imagen y reputación de la empresa.</li>
                  <li>Una clara diferenciación de la empresa respecto a sus competidores.</li>
                  <li>Una mayor participación de mercado.</li>
                  <li>Una mejora en la administraci&oacute;n de los recursos de la empresa.</li>
                </ul>
              </p>
              <div class="text-center">
                <a href="#Contacto" class="btn btn-warning btn-raised mr-1 wow flipInX animation-delay-14">
                  <i class="zmdi zmdi-email"></i> Contacto </a>
              </div>
            </div>
          </div>
        </div>
      </div>

        </section>
        <section id="Servicios" class="">
           <div class="container mt-6">
        <h1 class="font-light">Nuestros Servicios.</h1>
        <p class="lead color-primary">“Somos lo que hacemos día a día. De modo que la excelencia no es un acto sino un hábito”. - Aristóteles </p>
        <div class="panel panel-light panel-flat">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs nav-tabs-transparent indicator-primary nav-tabs-full nav-tabs-5" role="tablist">
            <li class="nav-item wow fadeInDown animation-delay-6" role="presentation">
              <a href="#windows" aria-controls="windows" role="tab" data-toggle="tab" class="nav-link withoutripple">
                <i class="zmdi zmdi-phone"></i>
                <span class="d-none d-md-inline">Telefon&iacute;a VoIp</span>
              </a>
            </li>
            <li class="nav-item wow fadeInDown animation-delay-4" role="presentation">
              <a href="#macos" aria-controls="macos" role="tab" data-toggle="tab" class="nav-link withoutripple active">
                <i class="fa fa-linux"></i>
                <span class="d-none d-md-inline">Desarrollo de software</span>
              </a>
            </li>
            <li class="nav-item wow fadeInDown animation-delay-2" role="presentation">
              <a href="#linux" aria-controls="linux" role="tab" data-toggle="tab" class="nav-link withoutripple">
                <i class="zmdi zmdi-smartphone-iphone"></i>
                <span class="d-none d-md-inline">Desarrollo m&oacute;vil</span>
              </a>
            </li>
            <li class="nav-item wow fadeInDown animation-delay-4" role="presentation">
              <a href="#android" aria-controls="android" role="tab" data-toggle="tab" class="nav-link withoutripple">
                <i class="zmdi zmdi-desktop-mac"></i>
                <span class="d-none d-md-inline">Desarrollo Web</span>
              </a>
            </li>
            <li class="nav-item wow fadeInDown animation-delay-6" role="presentation">
              <a href="#ios" aria-controls="ios" role="tab" data-toggle="tab" class="nav-link withoutripple">
                <i class="zmdi zmdi-wrench"></i>
                <span class="d-none d-md-inline">Soporte.</span>
              </a>
            </li>
          </ul>
          <div class="panel-body">
            <!-- Tab panes -->
            <div class="tab-content mt-4">
              <div role="tabpanel" class="tab-pane fade" id="windows">
                <div class="row">
                  <div class="col-lg-6 order-lg-2">
                    <img src="assets/img/demo/mock4.png" alt="" class="img-fluid animated zoomIn animation-delay-8"> </div>
                  <div class="col-lg-6 order-lg-1">
                    <h3 class="text-normal animated fadeInUp animation-delay-4">VoIP</h3>
                    <p class="lead lead-md animated fadeInUp animation-delay-6">
                      Con VoIP uno puede realizar una llamada desde cualquier lado que exista conectividad a internet. Dado que los teléfonos IP transmiten su información a trabes de internet estos pueden ser administrados por su proveedor desde cualquier lugar donde exista una conexión. Esto es una ventaja para las personas que suelen viajar mucho, estas personas pueden llevar su teléfono consigo siempre teniendo acceso a su servicio de telefonía IP.
                    </p>
                    <p class="lead lead-md animated fadeInUp animation-delay-7">
                      
                    </p>
                    <div class="">
                      <!--a href="javascript:void(0)" class="btn btn-info btn-raised animated zoomIn animation-delay-10">
                        <i class="zmdi zmdi-info"></i> Ver m&aacute;s</a-->
                      <a href="#Contacto" class="btn btn-danger btn-raised mr-1 animated zoomIn animation-delay-12">
                        <i class="zmdi zmdi-chart-donut"></i> Contacto </a>
                    </div>
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane active show fade" id="macos">
                <div class="row">
                  <div class="col-lg-6">
                    <img src="assets/img/demo/mock2.png" alt="" class="img-fluid wow animated zoomIn animation-delay-8"> </div>
                  <div class="col-lg-6">
                    <h3 class="text-normal wow animated fadeInUp animation-delay-4">Desarrollo de software a la medida.</h3>
                    <p class="lead lead-md  wow animated fadeInUp animation-delay-6">Esto es fundamental para el negocio de productos y procesos digitales. La incorporación profunda de la tecnología creará puntos de Contacto para los usuarios de todo el mundo y asentará las bases del negocio digital.</p>
                    <p class="lead lead-md wow animated fadeInUp animation-delay-7">El desarrollo de software a la medida, se hace para una empresa o usuario específico, basado en las necesidades y cumpliendo con características específicas. El en el día a día de las empresas, contar con un software a la medida es un <strong>activo</strong> muy importante que permite realizar de una forma más efectiva las actividades en busca de cumplir los objetivos del negocio.</p>
                    <div class="">
                      <!--a href="javascript:void(0)" class="btn btn-info btn-raised wow animated zoomIn animation-delay-10">
                        <i class="zmdi zmdi-eye"></i> Ver m&aacute;s</a-->
                      <a href="#Contacto" class="btn btn-danger btn-raised mr-1 wow animated zoomIn animation-delay-12">
                        <i class="zmdi zmdi-email"></i> Contacto </a>
                    </div>
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="linux">
                <div class="row">
                  <div class="col-lg-6 order-lg-2">
                    <img src="assets/img/demo/mock5.png" alt="" class="img-fluid animated zoomIn animation-delay-8"> </div>
                  <div class="col-lg-6 order-lg-1">
                    <h3 class="text-normal animated fadeInUp animation-delay-4">Desarrollo m&oacute;vil.</h3>
                    <p class="lead lead-md animated fadeInUp animation-delay-6">2018 se presenta como un año cargado de razones para seguir con el desarrollo de aplicaciones móviles. Promete ser un año en donde se contin&uacute;e la racha de crecimiento en el uso de las apps móviles. También destacarán más marcadamente tendencias que ya hemos visto aparecer en años precedentes.</p>
                    <p class="lead lead-md animated fadeInUp animation-delay-7">La tecnología abre nuevas oportunidades de potenciar nuestro negocio por medio de las aplicaciones móviles. Descubre cuáles son sus ventajas y por qué deberías crear una. ¡Adelante!.</p>
                    <div class="">
                      <!--a href="javascript:void(0)" class="btn btn-info btn-raised animated zoomIn animation-delay-10">
                        <i class="zmdi zmdi-eye"></i> Ver m&aacute;s</a-->
                      <a href="#Contacto" class="btn btn-danger btn-raised mr-1 animated zoomIn animation-delay-12">
                        <i class="zmdi zmdi-email"></i> Contacto </a>
                    </div>
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="android">
                <div class="row">
                  <div class="col-lg-6">
                    <img src="assets/img/demo/mock6.png" alt="" class="img-fluid animated zoomIn animation-delay-8"> </div>
                  <div class="col-lg-6">
                    <h3 class="text-normal animated fadeInUp animation-delay-4">Diseño y Desarrollo Web</h3>
                    <p class="lead lead-md  animated fadeInUp animation-delay-6">
                      Existen muchas razones por las cuales usted debe tener un sitio web para su empresa o negocio. Hoy en día el internet es una herramienta utilizada en todo el mundo, y nos permite acceder a múltiples recursos y conocer sobre otras empresas con facilidad. Muchas empresas crecen exponencialmente gracias a sus sitios web. Es hora de que usted aproveche esta herramienta de comunicación para potenciar su empresa al máximo.
                    </p>
                    <div class="">
                      <!--a href="javascript:void(0)" class="btn btn-info btn-raised animated zoomIn animation-delay-10">
                        <i class="zmdi zmdi-eye"></i> Ver m&aacutes;</a-->
                      <a href="#Contacto" class="btn btn-danger btn-raised mr-1 animated zoomIn animation-delay-12">
                        <i class="zmdi zmdi-email"></i> Contacto </a>
                    </div>
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="ios">
                <div class="row">
                  <div class="col-lg-6 order-lg-2">
                    <img src="assets/img/demo/mock3.png" alt="" class="img-fluid animated zoomIn animation-delay-8"> </div>
                  <div class="col-lg-6 order-lg-1">
                    <h3 class="text-normal animated fadeInUp animation-delay-4">Soporte t&eacute;cnico</h3>
                    <p class="lead lead-md animated fadeInUp animation-delay-6">Atenci&oacute;n especializada.</p>
                    <p class="lead lead-md animated fadeInUp animation-delay-6">Con menor tiempo de respuesta.</p>
                    <p class="lead lead-md animated fadeInUp animation-delay-6">Si usted ya cuenta con sistemas o call center proporcionamos mantenimientos preventivos y correctivos seg&uacute;n sea el caso.</p>
                    <div class="">
                      <!--a href="javascript:void(0)" class="btn btn-info btn-raised animated zoomIn animation-delay-10">
                        <i class="zmdi zmdi-eye"></i> Ver m&aacute;s</a-->
                      <a href="#Contacto" class="btn btn-danger btn-raised mr-1 animated zoomIn animation-delay-12">
                        <i class="zmdi zmdi-email"></i> Contacto </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- panel -->
      </div>
        </section>

        <section id="Clientes">
          <div class="wrap ms-hero-img-airplane ms-hero-bg-royal ms-bg-fixed">
            <div class="container">
          <h2 class="text-center no-m" style="color: #02a9f4">Nuestros clientes</h2>
          <div class="owl-dots"></div>
          <div class="owl-carousel owl-theme">
            <div class="card animation-delay-6">
              <div class="withripple zoom-img">
                <a href="javascript:void()">
                  <img src="assets/img/demo/port4.jpg" alt="..." class="img-fluid">
                </a>
              </div>
              <div class="card-block">
                <h3 class="color-primary">Thumbnail label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, repellat, vitae porro ex expedita cumque nulla.</p>
                <p class="text-right">
                  <a href="javascript:void()" class="btn btn-primary btn-raised text-right" role="button">
                    <i class="zmdi zmdi-collection-image-o"></i> View More</a>
                </p>
              </div>
            </div>
            <div class="card card-dark-inverse animation-delay-8">
              <div class="withripple zoom-img">
                <a href="javascript:void()">
                  <img src="assets/img/demo/port24.jpg" alt="..." class="img-fluid">
                </a>
              </div>
              <div class="card-block">
                <h3 class="">Thumbnail label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, repellat, vitae porro ex expedita cumque nulla.</p>
                <p class="text-right">
                  <a href="javascript:void()" class="btn btn-info btn-raised text-right" role="button">
                    <i class="zmdi zmdi-collection-image-o"></i> View More</a>
                </p>
              </div>
            </div>
            <div class="card animation-delay-10">
              <div class="withripple zoom-img">
                <a href="javascript:void()">
                  <img src="assets/img/demo/port7.jpg" alt="..." class="img-fluid">
                </a>
              </div>
              <div class="card-block">
                <h3 class="color-primary">Thumbnail label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, repellat, vitae porro ex expedita cumque nulla.</p>
                <p class="text-right">
                  <a href="javascript:void()" class="btn btn-primary btn-raised text-right" role="button">
                    <i class="zmdi zmdi-collection-image-o"></i> View More</a>
                </p>
              </div>
            </div>
            <div class="card animation-delay-6">
              <div class="withripple zoom-img">
                <a href="javascript:void()">
                  <img src="assets/img/demo/port8.jpg" alt="..." class="img-fluid">
                </a>
              </div>
              <div class="card-block">
                <h3 class="color-primary">Thumbnail label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, repellat, vitae porro ex expedita cumque nulla.</p>
                <p class="text-right">
                  <a href="javascript:void()" class="btn btn-primary btn-raised text-right" role="button">
                    <i class="zmdi zmdi-collection-image-o"></i> View More</a>
                </p>
              </div>
            </div>
            <div class="card card-dark-inverse animation-delay-8">
              <div class="withripple zoom-img">
                <a href="javascript:void()">
                  <img src="assets/img/demo/port9.jpg" alt="..." class="img-fluid">
                </a>
              </div>
              <div class="card-block">
                <h3 class="">Thumbnail label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, repellat, vitae porro ex expedita cumque nulla.</p>
                <p class="text-right">
                  <a href="javascript:void()" class="btn btn-info btn-raised text-right" role="button">
                    <i class="zmdi zmdi-collection-image-o"></i> View More</a>
                </p>
              </div>
            </div>
            <div class="card animation-delay-10">
              <div class="withripple zoom-img">
                <a href="javascript:void()">
                  <img src="assets/img/demo/port5.jpg" alt="..." class="img-fluid">
                </a>
              </div>
              <div class="card-block">
                <h3 class="color-primary">Thumbnail label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, repellat, vitae porro ex expedita cumque nulla.</p>
                <p class="text-right">
                  <a href="javascript:void()" class="btn btn-primary btn-raised text-right" role="button">
                    <i class="zmdi zmdi-collection-image-o"></i> View More</a>
                </p>
              </div>
            </div>
            <div class="card animation-delay-6">
              <div class="withripple zoom-img">
                <a href="javascript:void()">
                  <img src="assets/img/demo/port11.jpg" alt="..." class="img-fluid">
                </a>
              </div>
              <div class="card-block">
                <h3 class="color-primary">Thumbnail label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, repellat, vitae porro ex expedita cumque nulla.</p>
                <p class="text-right">
                  <a href="javascript:void()" class="btn btn-primary btn-raised text-right" role="button">
                    <i class="zmdi zmdi-collection-image-o"></i> View More</a>
                </p>
              </div>
            </div>
            <div class="card card-dark-inverse animation-delay-8">
              <div class="withripple zoom-img">
                <a href="javascript:void()">
                  <img src="assets/img/demo/port3.jpg" alt="..." class="img-fluid">
                </a>
              </div>
              <div class="card-block">
                <h3 class="">Thumbnail label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, repellat, vitae porro ex expedita cumque nulla.</p>
                <p class="text-right">
                  <a href="javascript:void()" class="btn btn-info btn-raised text-right" role="button">
                    <i class="zmdi zmdi-collection-image-o"></i> View More</a>
                </p>
              </div>
            </div>
            <div class="card animation-delay-10">
              <div class="withripple zoom-img">
                <a href="javascript:void()">
                  <img src="assets/img/demo/port14.jpg" alt="..." class="img-fluid">
                </a>
              </div>
              <div class="card-block">
                <h3 class="color-primary">Thumbnail label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, repellat, vitae porro ex expedita cumque nulla.</p>
                <p class="text-right">
                  <a href="javascript:void()" class="btn btn-primary btn-raised text-right" role="button">
                    <i class="zmdi zmdi-collection-image-o"></i> View More</a>
                </p>
              </div>
            </div>
          </div>
        </section>

        <?php //require "envia_correo_contacto.php"; 
          $nombre  = 0;
          $email   = 0;
          $asunto  = 0;
          $mensaje = 0;

          if ($_POST['inputName']) {
            $nombre  = $_POST['inputName'];
          };

          if ($_POST['inputEmail']) {
            $email   = $_POST['inputEmail'];
          };

          if ($_POST['inputSubject']) {
            $asunto  = $_POST['inputSubject'];
          };

          if ($_POST['textArea']) {
            $mensaje  = $_POST['textArea'];
          };

          #$nombre  = $_POST['inputName'];
          #$email   = $_POST['inputEmail'];
          #$asunto  = $_POST['inputSubject'];
          #$mensaje = $_POST['textArea']; 

          if ($nombre != "0" && $email != "0" && $asunto != "0" && $mensaje != "0") {
            $message = $nombre . " " . $email . " " .$mensaje; 
            if(mail("contacto@ecmaster.mx", $asunto, $message)){
              #echo "Mensaje envíado correctamente";
              $msg = 1;
            }else {
              $msg = 0;
              #echo "Error al enviar, intente más tarde";
            } 
          }else{
            #echo "tiene 0";
          }
        ?>

        <section id="Contacto" class="mt-6">
          <div class="wrap ms-hero-bg-warning ms-hero-img-team ms-bg-fixed">
            <div class="container">
              <h1 class="text-center color-white mb-4 wow fadeInUp animation-delay-2">Contacto</h1>
              <div class="row">
                <div class="col-lg-12">
                  <div class="card card-primary animated zoomInUp animation-delay-5">
                    <div class="card-body">
                      <form class="form-horizontal" id="contacto-ecmaster" method="POST">
                        <?php if ($msg === 1) { ?>
                          <div id="resultado" class="col-lg-12">
                            <p>
                              <strong>
                                <h4 style="color:#4caf50;">
                                  El mensaje se envío correctamente.
                                </h4>
                              </strong>
                            </p>
                          </div>
                        <?php } ?>
                        <?php if ($msg === 0) { ?>
                          <div id="resultado" class="col-lg-12">
                            <p>
                              <strong>
                                <h4 style="color:#f44336;">
                                  *Error al enviar, intente más tarde.
                                </h4>
                              </strong>
                            </p>
                          </div>
                        <?php } ?>
                        <fieldset class="container">
                          <div class="form-group row">
                            <label for="inputEmail" autocomplete="false" class="col-lg-2 control-label">Name</label>
                            <div class="col-lg-9">
                              <input type="text" class="form-control" id="inputName" name="inputName" placeholder="Name" required=""> </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputEmail" autocomplete="false" class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-9">
                              <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email"> </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputEmail" autocomplete="false" class="col-lg-2 control-label">Subject</label>
                            <div class="col-lg-9">
                              <input type="text" class="form-control" id="inputSubject" name="inputSubject" placeholder="Subject"> </div>
                          </div>
                          <div class="form-group row">
                            <label for="textArea" class="col-lg-2 control-label">Message</label>
                            <div class="col-lg-9">
                              <textarea class="form-control" rows="3" id="textArea" name="textArea" placeholder="Yout message..."></textarea>
                            </div>
                          </div>
                          <div class="form-group row justify-content-end">
                            <div class="col-lg-10">
                              <button type="submit" class="btn btn-raised btn-primary">Submit</button>
                              <button type="button" class="btn btn-danger">Cancel</button>
                            </div>
                          </div>
                        </fieldset>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- container -->
          </div>
          <!--iframe width="100%" height="340" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d48342.06480344582!2d-73.980069429762!3d40.775680208459505!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2589a018531e3%3A0xb9df1f7387a94119!2sCentral+Park!5e0!3m2!1sen!2sus!4v1491233314840"></iframe-->
        </section>

              <aside class="ms-footbar">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 col-md-4 ms-footer-col">
              <div class="ms-footbar-block">
                <h3 class="ms-footbar-title">Sitemap</h3>
                <ul class="list-unstyled ms-icon-list three_cols">
                  <li>
                    <a href="#Inicio">
                      <i class="zmdi zmdi-home"></i> Inicio</a>
                  </li>
                  <li>
                    <a href="#Nosotros">
                      <i class="zmdi zmdi-favorite-outline"></i> Nostros</a>
                  </li>
                  <li>
                    <a href="#Servicios">
                      <i class="zmdi zmdi-face"></i> Servicios</a>
                  </li>
                  <li>
                    <a href="#Clientes">
                      <i class="zmdi zmdi-case"></i> Clientes</a>
                  </li>
                  <li>
                    <a href="#Contacto">
                      <i class="zmdi zmdi-email"></i> Contacto</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 ms-footer-col ms-footer-alt-color">
              <div class="ms-footbar-block">
                <h3 class="ms-footbar-title text-center mb-2">Redes Sociales</h3>
                <div class="ms-footer-media text-center">
              
                  <div class="ms-footbar-social">
                    <a href="javascript:void(0)" class="btn-circle btn-facebook">
                      <i class="zmdi zmdi-facebook"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn-circle btn-twitter">
                      <i class="zmdi zmdi-twitter"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn-circle btn-youtube">
                      <i class="zmdi zmdi-youtube-play"></i>
                    </a>
                    <br>
                    <a href="javascript:void(0)" class="btn-circle btn-google">
                      <i class="zmdi zmdi-google"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn-circle btn-instagram">
                      <i class="zmdi zmdi-instagram"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn-circle btn-github">
                      <i class="zmdi zmdi-github"></i>
                    </a>
                  </div>
              
 
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 ms-footer-col ms-footer-text-right">
              <div class="ms-footbar-block">
                <div class="ms-footbar-title">
                  <span class="ms-logo ms-logo-white ms-logo-sm mr-1">EC</span>
                  <h3 class="no-m ms-site-title">
                    <span>ECmaster</span>
                  </h3>
                </div>
                <address class="no-mb">
                  <p>
                    <i class="color-info-light zmdi zmdi-email mr-1"></i>
                    <a href="mailto:example@example.com">example@domain.com</a>
                  </p>
                  <p>
                    <i class="color-royal-light zmdi zmdi-phone mr-1"></i>55 6853 3844 </p>
                  <p>
                    <i class="color-royal-light zmdi zmdi-phone mr-1"></i>55 9194 5409 </p>
                </address>
              </div>
            </div>
          </div>
        </div>
      </aside>
        <footer class="ms-footer">
          <div class="container">
            <p>Copyright &copy; ECmaster 2018</p>
          </div>
        </footer>
        <div class="btn-back-top">
          <a href="#" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised ">
            <i class="zmdi zmdi-long-arrow-up"></i>
          </a>
        </div>
      </div>
    </div>
    <!-- ms-site-container -->
    <div class="ms-slidebar sb-slidebar sb-left sb-momentum-scrolling sb-style-overlay">
      <header class="ms-slidebar-header">
        <div class="ms-slidebar-title">
          <div class="ms-slidebar-t">
            <span class="ms-logo ms-logo-sm">EC</span>
            <h3>ECmaster</h3>
          </div>
        </div>
      </header>
      <ul class="ms-slidebar-menu" id="slidebar-menu" role="tablist" aria-multiselectable="true">
        <li>
          <a data-scroll class="link" href="#Inicio">
            <i class="zmdi zmdi-Inicio"></i> Inicio</a>
        </li>
        <li>
          <a data-scroll class="link" href="#Nosotros">
            <i class="zmdi zmdi-flight-takeoff"></i> Nosotros</a>
        </li>
        <li>
          <a data-scroll class="link" href="#Servicios">
            <i class="zmdi zmdi-desktop-mac"></i> Servicios</a>
        </li>
        <li>
          <a data-scroll class="link" href="#Clientes">
            <i class="zmdi zmdi-money-box"></i> Clientes</a>
        </li>
        <li>
          <a data-scroll class="link" href="#Contacto">
            <i class="zmdi zmdi-email"></i> Contacto</a>
        </li>
      </ul>
      <div class="ms-slidebar-social ms-slidebar-block">
        <h4 class="ms-slidebar-block-title">Social Links</h4>
        <div class="ms-slidebar-social">
          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm btn-facebook">
            <i class="zmdi zmdi-facebook"></i>
          </a>
          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm btn-twitter">
            <i class="zmdi zmdi-twitter"></i>
          </a>
          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm btn-youtube">
            <i class="zmdi zmdi-youtube"></i>
          </a>
          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm btn-instagram">
            <i class="zmdi zmdi-instagram"></i>
          </a>
        </div>
      </div>
    </div>
    <script src="assets/js/plugins.min.js"></script>
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/configurator.min.js"></script>
    <script>
      (function(i, s, o, g, r, a, m)
      {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function()
        {
          (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
          m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
      })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-90917746-2', 'auto');
      ga('send', 'pageview');
    </script>
    
    <script src="assets/js/lead-full.js"></script>
    <script src="assets/js/index.js"></script>
  </body>
</html>